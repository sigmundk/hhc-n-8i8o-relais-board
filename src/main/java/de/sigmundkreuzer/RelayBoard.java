package de.sigmundkreuzer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;

public class RelayBoard implements AutoCloseable {
    private Socket socket;

    private RelayBoard(Socket socket) {
        this.socket = socket;
    }

    public static RelayBoard connect(InetSocketAddress address) throws IOException {
        Socket socket = new Socket();
        socket.setTcpNoDelay(true);
        socket.connect(address, 30000);
        read(socket);
        return new RelayBoard(socket);
    }

    private static String read(Socket socket) throws IOException {
        InputStreamReader inFromRelais = new InputStreamReader(socket.getInputStream());
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();

        int rsz = inFromRelais.read(buffer, 0, buffer.length);
        if (rsz < 0)
            throw new IllegalStateException("Nothing to read from RelayBoard");
        out.append(buffer, 0, rsz);
        System.out.println("Read:" + out.toString());
        return out.toString();
    }

    public void pulse(int relaisNumber, int time) throws IOException, InterruptedException {
        if (time > 99 || time < 1) {
            throw new InvalidParameterException("Time must be from 1 to 99");
        }
        String command = String.format("on%d:%02d", relaisNumber, time);
        sendCommand(command);
    }

    public void setOn(int relaisNumber) throws IOException, InterruptedException {
        String command = String.format("on%d", relaisNumber);
        sendCommandAck(command);
    }

    public void setOff(int relaisNumber) throws IOException, InterruptedException {
        String command = String.format("off%d", relaisNumber);
        sendCommandAck(command);
    }

    private void sendCommandAck(String command) throws IOException, InterruptedException {
        sendCommand(command);
        validateAck(command);
    }

    private void validateAck(String command) throws IOException {
        String ack = read(this.socket);
        if (!ack.equals(command)) {
            throw new IllegalStateException("Expected answer from relais is [" + command + "] but got [" + ack + "]");
        }
    }

    private void sendCommand(String command) throws IOException, InterruptedException {
        DataOutputStream outToRelais = new DataOutputStream(this.socket.getOutputStream());
        System.out.println("Send:" + command);
        byte[] bytes = command.getBytes(StandardCharsets.US_ASCII);
        outToRelais.write(bytes);
        outToRelais.flush();
        Thread.sleep(5);// The board seems to have problems if you send multiple commands without waiting. 1ms seems to be enougth, to be sure i use 5
    }

    @Override
    public void close() throws Exception {
        socket.close();
    }
}
