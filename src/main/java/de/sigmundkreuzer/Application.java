package de.sigmundkreuzer;

import java.net.InetSocketAddress;

public class Application {

    public static void main(String[] args) {
        try (RelayBoard r = RelayBoard.connect(new InetSocketAddress("192.168.0.105", 5000))) {

            // Turn on one relais after the other for 1 sec
            r.setOn(1);
            Thread.sleep(1000);
            for (int i = 2; i <= 8; i++) {
                r.setOn(i);
                r.setOff(i - 1);
                Thread.sleep(1000);
            }
            r.setOff(8);
            Thread.sleep(1000);

            // Turn on one relais after the other for 2 sec with the timed on function
            for (int i = 1; i <= 8; i++) {
                r.pulse(i, 2);
                Thread.sleep(1000);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}


