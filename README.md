# HHC-N-8I8O Relais board


Simple example how to use the HHC-N-8I8O Relais board with java.
Maybe it work also with the HHC-N-4I4O (i cannot test it)

My relais board(V1.4 from 2018-09-01) was configured to listen on IP 192.168.0.105:5000-5004 by default.

**You can also test it with a Linux commandline(Just press Ctrl + C to close nc)**

Test if the board response
`nc 192.168.0.105 5000`
It should return "HHC-N-8I8O"


Turn Relais 1 on 
`echo -n 'on1' | nc 192.168.0.105 5000`


Turn Relais 1 off
`echo -n 'off1' | nc 192.168.0.105 5000`
